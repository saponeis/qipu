"""
@author:: Jotagê Sales
@organization: FS Company
@copyright: 2018 fs.com.br todos os direitos reservados.
"""
from unittest import TestCase

import requests_mock
from bs4 import BeautifulSoup

from qipu.aisweb import AisWeb
from tests import fake_page


class AisWebTestCase(TestCase):

    def setUp(self):
        self.icao_code = 'TEST'
        self.url = 'https://www.aisweb.aer.mil.br/?i=aerodromos&codigo={}'.format(self.icao_code)
        self.aisweb = AisWeb(self.icao_code)

    def test_normalize_string_broken_line(self):
        text = self.aisweb._normalize('jotage sales \n')
        self.assertEqual(text, 'jotage sales')

    def test_normalize_string_without_broken_line(self):
        text = self.aisweb._normalize('jotage sales')
        self.assertEqual(text, 'jotage sales')

    def test_normalize_string_with_space_and_tab(self):
        text = self.aisweb._normalize('jotage sales     \t\t  ')
        self.assertEqual(text, 'jotage sales')

    @requests_mock.mock()
    def test_extract_metar(self, mock_site):
        mock_site.get(self.url, content=fake_page, status_code=200)
        self.aisweb.load()

        expected = '082100Z 09008KT 5000 -RA BR BKN008 OVC012 18/16 Q1021='

        self.assertEqual(self.aisweb.metar, expected)

    @requests_mock.mock()
    def test_extract_metar_index_error(self, mock_site):
        mock_site.get(self.url, content=b'', status_code=200)
        self.aisweb.load()

        with self.assertRaises(IndexError):
            self.aisweb.metar

    @requests_mock.mock()
    def test_extract_taf(self, mock_site):
        mock_site.get(self.url, content=fake_page, status_code=200)
        self.aisweb.load()

        expected = '081616Z 0818/0906 12007KT 9000 BKN015TX18/0818Z TN16/0823ZPROB30 0818/0823 8000 DZ OVC010BECMG' \
                   ' 0823/0901 BKN009 RMK PHM='

        self.assertEqual(self.aisweb.taf, expected)

    @requests_mock.mock()
    def test_extract_taf_index_error(self, mock_site):
        mock_site.get(self.url, content=b'', status_code=200)
        self.aisweb.load()

        with self.assertRaises(IndexError):
            self.aisweb.taf

    @requests_mock.mock()
    def test_type_object_on_parse_html(self, mock_site):
        mock_site.get(self.url, content=fake_page, status_code=200)
        self.aisweb.load()

        self.assertTrue(isinstance(self.aisweb.page, BeautifulSoup))

    @requests_mock.mock()
    def test_return_property_charts(self, mock_site):
        mock_site.get(self.url, content=fake_page, status_code=200)
        self.aisweb.load()

        self.assertTrue(isinstance(self.aisweb.charts, list))
        self.assertTrue(isinstance(self.aisweb.charts[0], dict))

    @requests_mock.mock()
    def test_content_property_charts(self, mock_site):
        mock_site.get(self.url, content=fake_page, status_code=200)
        self.aisweb.load()

        self.assertTrue('link' in self.aisweb.charts[0])
        self.assertTrue('name' in self.aisweb.charts[0])
