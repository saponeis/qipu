FROM python:3.6.2-alpine
MAINTAINER Jotagê Sales <jotage_sales@hotmail.com>
LABEL version="1.0.0"

RUN apk add --no-cache --update bash build-base
RUN mkdir src
WORKDIR src

ADD qipu ./qipu
ADD tests ./tests
ADD qipu_script_1.py .
ADD qipu_script_2.py .
ADD Makefile .
ADD requirements.txt .

RUN pip install -U pip setuptools
RUN pip install -r requirements.txt