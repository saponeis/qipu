"""
@author:: Jotagê Sales
@organization: FS Company
@copyright: 2018 fs.com.br todos os direitos reservados.
"""
import re

import requests
from bs4 import BeautifulSoup


class AisWeb:
    base_url = 'https://www.aisweb.aer.mil.br'

    def __init__(self, icao_code):
        self.icao_code = icao_code.upper()

    def __repr__(self):
        return 'AisWeb'

    @property
    def sun_information(self):
        sunrise = self.page.select_one('sunrise').text
        sunset = self.page.select_one('sunset').text
        return {'sunrise': sunrise, 'sunset': sunset}

    @property
    def metar(self):
        metar = self.page.select('div.col-lg-4.order-sm-12 p')[2].text
        return self._normalize(metar)

    @property
    def taf(self):
        taf = self.page.select('div.col-lg-4.order-sm-12 p')[3].text
        return self._normalize(taf)

    @property
    def charts(self):
        charts = []
        for chart in self.page.select('div.col-lg-4.order-sm-12 ul li a'):
            data = {}
            data['link'] = chart.get('href')
            data['name'] = chart.text
            charts.append(data)
        return charts

    def parse_html(self, response):
        self.page = BeautifulSoup(response.content, 'html.parser')

    def make_request(self, url):
        response = requests.get(url)
        self.parse_html(response)

    def load(self):
        url = '{}/?i=aerodromos&codigo={}'.format(self.base_url, self.icao_code)
        self.make_request(url)

    def _normalize(self, text):
        return re.sub(r'[\\n|\\t|\s]+$', '', text)
